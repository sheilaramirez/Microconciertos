from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import *
from kivy.lang import Builder
from kivy.uix.textinput import TextInput

Builder.load_file('kv_file.kv')



class MainApp(App):
    def build(self):
        sm=ScreenManager()
        sm.add_widget(Principal(name='begin'))
        sm.add_widget(Search(name='search'))
        sm.add_widget(Plus(name='plus'))
        return sm

class Principal(Screen):
    pass
class Search(Screen):
    def Go_Search(self):
        pass
class Plus(Screen):
   def Send(self):
        pass
    
if __name__=='__main__':
    MainApp().run()
